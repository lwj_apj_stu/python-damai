# python-damai

#### 介绍
仅供学习，拒绝商业
网页版可抢票
h5需要自己改下域名 路径
没有自动选座，我不会 哈哈哈哈哈，手动选吧，但是选座人数 == 购买人数会自动提交订单

#### 软件架构

python-seleium


#### 安装教程

下载直接  直接运行main

#### 使用说明

系统:win10

python 版本:3.8.10

需要安装的库:selenium

安装方法: pip install selenium

因为我使用的是谷歌浏览器，所以用的是谷歌浏览器的驱动。

查看自己谷歌版本号：右上角三个点，帮助，关于Google Chrome

谷歌浏览器驱动地址 自己谷歌赵

如果你的版本没有，那么则下载你的最近你的版本比你大的，然后把驱动放在python的根目录

我的python安装在：C:\Users\WIN10\AppData\Local\Programs\Python 所以放在这个目录

然后系统环境把这个路径加进去。

当然也可以随便放，executable_path改成驱动的绝对路径/chromedriver.exe

self.driver = webdriver.Chrome(executable_path=self.driver_path,
options=self.chrome_options)

[ticket_info]

book_url= #购票网址

price=2 #票档位 1=索引0

name_num=1,2 #实名认证后 勾选的实名人选项 1 = 索引0  如果购买数量大于1，那么需要配置的实名勾选数量也得是数组 一般情况下 选座类型的不需要配置这个

buy_num=2 #购买票数

num=3 #抢票次数

date_time=2023-4-22 22:38:00 #抢票时间

session=1 #场次时间   城市切换重新配置book_url就行 一个城市一个book_url 

[setting]

shiming=1 #是否需要勾选实名信息

xuanzuo=0 #是否选座


[other]

driver_path=./chromedriver.exe
